/**************/
/* GfTuple.go */
/**************/

package common

import "encoding"

// This interface defines the functions that can be used
// to retrieve elements from a fast tuple. A fast tuple
// is effectively a type-checked list of elements that
// are pre-defined as safe to be serialized or sent to
// cross-language operators.
type GfTuple interface {
    Copy() GfTuple
    GetSchema() []int

    GetString(int) (string, error)
    GetInt(int) (int, error)
    GetFloat(int) (float64, error)
    GetStringList(int) ([]string, error)
    GetIntList(int) ([]int, error)
    GetFloatList(int) ([]float64, error)
    GetTuple(int) (GfAssocTuple, error)

    SetString(int, string) error
    SetInt(int, int) error
    SetFloat(int, float64) error
    SetStringList(int, []string) error
    SetIntList(int, []int) error
    SetFloatList(int, []float64) error
    SetTuple(int, GfAssocTuple) error

    GetType(int) (int, bool)

    encoding.BinaryMarshaler
    encoding.BinaryUnmarshaler
}
