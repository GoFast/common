# GoFast Common Library

GoFast is an application and library to implement simple stream processing
and complex event processing either embedded within an application or as
a standalone service.

## Stream processing

Stream processing refers to analyzing data in motion, rather than data at
rest. This requires a different approach to function architecture than a
normal application.

A stream declaration is defined as a series of operators connected in a
directed graph. Often the graph is acyclic, but this is not necessarily
the case. Single messages pass through these operators and are processed
one at a time, similar to iterating through a never-ending series of
database rows.

An operator is composed of 4 pieces:

### Init

The initialization function is used to take in the GoFast engine context
and the parameters used to create the operator. It is solely used to get
things set up and ready to go.

### Start

The start function is called once all operators have been initialized.
It is used when operators should actually start sending tuples. For instance,
the start function is where a TCP server operator would actually begin
listening on its configured port.

### Process

The process function is called for each tuple that is sent to the operator.
In this function, the operator may submit 0 or more tuples, or it might
store the tuple to reference again at a later point in time (windowin).

### Stop

The stop function is called when the GoFast engine is shutting down. At this
point, the operator should clean up any pending actions, close database
connections and open files, and finalize any processing that needs to be
done.

### Example

    MyBeacon = go:Beacon(period: "1s", output: {"value1", 2});
    MyBeacon -> inline{`
        print(tuple[1])
        print(tuple[2])
    `};

The above snippet is a complete stream definition in the GoFast language.
First it creates an operator called `MyBeacon` that is a `Beacon` operator.
The `go:` prepended to `Beacon` tells us that the implementation of the
operator is in Golang. We set two parameters on this operator. The first
tells us that the operator should emit a message every second. The second
parameter tells us that the tuple that should be emitted consists of a string
"value1" and an integer 2.

The tuples are passed to an inline function (which is a stateless Lua operator)
that simply prints out the values of the tuple. As a reminder, Lua is 1-indexed,
so `[1]` correspnds to the string "value1".

## Complex Event Processing

General event processing just consists of a series of named events and actions
to be taken once those events occur. This doesn't take into account the
context sensitivity of events, though. For instance, if someone breaks into a
house, how would such a simple event system detect it in a smart home?

In GoFast, this event processing is easy:

    DoorOpened(`gofast.GetInt("IsAway") == 1`) then {`StartAlarm()`}
    -> AlarmDisabled within 30 seconds then {`
        Greet("Welcome home, " .. gofast.GetString("OwnerName"))
        gofast.SetInt("IsAway", 0)
    `} else {`
        Greet("Intruder detected. Notifying police")
        CallPolice()
    `};

What does this do? First, we listen for an event named `DoorOpened`. If this event
occurs and the filter passes (the owner's status is away, so we don't expect him/her),
then we proceed to the next step. We now listen for an event named `AlarmDisabled`.
We did not put a filter on this event, so any occurrence of it will match. We added
a clause to the end `within 30 seconds`. This means that the event must occur within
30 seconds of seeing the `DoorOpened` event. If the alarm is not disabled, then
the `else` clause is evaluated, which calls the police, because the smart home
can assume that there has been a break-in.

If the alarm is disabled within the time frame, then the action clause is executed,
which greets the owner by name and sets the away status to false.

This is good, but can we improve?

Let's say we want to have the option of detecting motion, and we also want to call
the owner to give him/her the option to disable the alarm remotely.

    DoorOpened(`gofast.GetInt("IsAway") == 1`) or MotionDetected(`gofast.GetInt("IsAway") == 1`)
    then {`
        StartAlarm(); CallOwner()
    `}
    -> AlarmDisabled within 30 seconds or PhoneAnswered within 1 minute then {`
        Greet("Welcome home, " .. gofast.GetString("OwnerName"))
        gofast.SetInt("IsAway", 0)
    `} else {`
        Greet("Intruder detected. Notifying police")
        CallPolice()
    `};

Let's break this down more. Again, we see the `DoorOpened` event with a filter. We
also see another trigger that can start this event chain. If either the
`MotionDetected` event or the `DoorOpened` event, the `then` clause for that phase
is evaluated. Then, we begin listening for the next phase. If the alarm is disabled
within 30 seconds or the owner disables the alarm via phone within 1 minute, then we
greet the owner and set the status of IsAway to false. After 30 seconds, the only
way this phase can be satisfied is with the owner remotely disabling the alarm. The
alarm being disabled will no longer be enough. Once the event phase cannot be
satisfied due to time limits, the `else` clause is evaluated, and the police are
called.
