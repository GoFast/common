/****************/
/* Constants.go */
/****************/

package common

// These constants define the schema tags for both
// associative tuples and fast tuples.
const (
	STRING_T int = iota
	INT_T
	FLOAT_T

	STRING_LIST_T
	INT_LIST_T
	FLOAT_LIST_T

	STRING_STRING_MAP_T
	STRING_INT_MAP_T
	STRING_FLOAT_MAP_T

	TUPLE_T

    NIL_T
)
