/*****************/
/* GfOperator.go */
/*****************/

package common

// This interface defines the functions that should be
// implemented by an operator. Any Golang operator
// needs to have these functions defined.
type GfOperator interface {
    Init(GfAssocTuple, GfContext) error
    Start() error
    Process(GfTuple)
    Stop() error
}
