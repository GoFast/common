/*******************/
/* GfAssocTuple.go */
/*******************/

package common

import "encoding"

// This defines the interface that an associative tuple
// should use. This is a type-checking map, effectively,
// so that various pre-defined "safe" types can be stored
// in the tuple for passing a set of parameters to an operator.
type GfAssocTuple interface {
    Copy() GfAssocTuple
    GetSchema() map[string]int

    GetString(string) (string, error)
    GetInt(string) (int, error)
    GetFloat(string) (float64, error)
    GetStringList(string) ([]string, error)
    GetIntList(string) ([]int, error)
    GetFloatList(string) ([]float64, error)
    GetTuple(string) (GfAssocTuple, error)

    SetString(string, string) error
    SetInt(string, int) error
    SetFloat(string, float64) error
    SetStringList(string, []string) error
    SetIntList(string, []int) error
    SetFloatList(string, []float64) error
    SetTuple(string, GfAssocTuple) error

    GetType(string) (int, bool)

    encoding.BinaryMarshaler
    encoding.BinaryUnmarshaler
}
