/****************/
/* GfContext.go */
/****************/

package common

// This defines the context of an operator, including
// a function to retrieve a new tuple with a given schema
// and a function to submit a tuple to one of the operator's
// output ports.
type GfContext interface {
    NewAssocTuple() GfAssocTuple
    NewTuple([]int) GfTuple
    Submit(int, GfTuple) bool
    Event(string, GfAssocTuple) bool
}
